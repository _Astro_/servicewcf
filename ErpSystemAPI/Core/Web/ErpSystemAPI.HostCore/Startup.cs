using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using ErpSystemAPI.Base.Resource;
using ErpSystemAPI.Service;
using ErpSystemAPI.ServiceContracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using SoapCore;

namespace ErpSystemAPI.HostCore
{
    public class Startup
    {
        /// <summary>
        /// Sekcja konfiguracji serwis�w
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.TryAddSingleton<IErpService, ErpService>();
            services.AddSoapCore();
            services.AddMvc();
        }

        /// <summary>
        /// Sekcja konfiguracji aplikacji
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints => 
            { 
                endpoints.UseSoapEndpoint<IErpService>(string.Format("/{0}.svc", AppStrings.ServiceNameCoreEndpoint), new BasicHttpBinding(), SoapSerializer.DataContractSerializer);
                endpoints.UseSoapEndpoint<IErpService>(string.Format("/{0}.asmx", AppStrings.ServiceNameCoreEndpoint), new BasicHttpBinding(), SoapSerializer.XmlSerializer);
            });
        }
    }
}
