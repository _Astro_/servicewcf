﻿using ErpSystemAPI.Base.Resource;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ErpSystemAPI.Base.Common
{
    /// <summary>
    /// Klasa wyliczeniowa dla statusu wyniku kontraktu
    /// </summary>
    [DataContract(Name = AppStrings.Status, Namespace = AppStrings.ServiceBasicNamespace)]
    public enum eResult
    {
        [EnumMember(Value = AppStrings.Success)]
        Success,
        [EnumMember(Value = AppStrings.Error)]
        Error
    }
}
