﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ErpSystemAPI.Base.Resource
{
    /// <summary>
    /// Klasa statyczna zamiast Resource - dla możliwości użycia zdefiniowanych stringów w atrybutach
    /// </summary>
    public static class AppStrings
    {
        public const string ServiceName = "ErpService";
        public const string ServiceNameCoreEndpoint = "ErpWebServiceCore";
        public const string ServiceContracts = "ErpServiceContract";
        public const string ServiceBasicNamespace = @"http://ErpSystemApi/ErpService/2020";
        public const string Action = "Action";
        public const string Status = "Status";
        public const string Message = "Message";
        public const string Success = "Success";
        public const string Error = "Error";
        public const string SalesOrderContractName = "SalesOrder";
        public const string SalesOrderRequestName = "SalesOrderRequest";
        public const string SalesOrderResponseName = "SalesOrderResponse";
        public const string SalesInvoiceContractName = "SalesInvoice";
        public const string SalesInvoiceRequestName = "SalesInvoiceRequest";
        public const string SalesInvoiceResponseName = "SalesInvoiceResponse";
        public const string PurchaseOrderContractName = "PurchaseOrder";
        public const string PurchaseOrderRequestName = "PurchaseOrderRequest";
        public const string PurchaseOrderResponseName = "PurchaseOrderResponse";
        public const string PurchaseInvoiceContractName = "PurchaseInvoice";
        public const string PurchaseInvoiceRequestName = "PurchaseInvoiceRequest";
        public const string PurchaseInvoiceResponseName = "PurchaseInvoiceResponse";
        public const string AResponseBaseClassName = "AResponseBase";

    }
}
