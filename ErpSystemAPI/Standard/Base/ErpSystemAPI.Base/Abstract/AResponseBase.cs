﻿using ErpSystemAPI.Base.Common;
using ErpSystemAPI.Base.Resource;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace ErpSystemAPI.Base.Abstract
{
    /// <summary>
    /// Bazowa klasa odpowiedzi
    /// </summary>
    [MessageContract(WrapperName = AppStrings.AResponseBaseClassName, WrapperNamespace = AppStrings.ServiceBasicNamespace)]
    public abstract class AResponseBase
    {
        [MessageBodyMember(Name = AppStrings.Status)]
        public eResult Status { get; set; }

        [MessageBodyMember(Name = AppStrings.Message)]
        public string Message { get; set; }
    }
}
