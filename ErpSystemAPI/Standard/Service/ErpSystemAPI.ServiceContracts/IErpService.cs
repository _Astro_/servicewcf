﻿using ErpSystemAPI.Base.Resource;
using ErpSystemAPI.MessageContracts.PurchaseInvoice;
using ErpSystemAPI.MessageContracts.PurchaseOrder;
using ErpSystemAPI.MessageContracts.SalesInvoice;
using ErpSystemAPI.MessageContracts.SalesOrder;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace ErpSystemAPI.ServiceContracts
{
    /// <summary>
    /// Interfejs głównej klasy serwisu z kontraktami
    /// </summary>
    [ServiceContract(
        Name = AppStrings.ServiceContracts,
        Namespace = AppStrings.ServiceBasicNamespace,
        SessionMode = SessionMode.Allowed)]
    public interface IErpService
    {
        [OperationContract(
            Action = "http://ErpSystemApi/ErpService/2020/ErpServiceContract/SalesOrder",
            ReplyAction = AppStrings.Action,
            IsTerminating = false,
            IsInitiating = true,
            IsOneWay = false,
            AsyncPattern = false)]
        SalesOrderResponse SalesOrder(SalesOrderRequest salesOrderRequest);

        [OperationContract(
            Action = "http://ErpSystemApi/ErpService/2020/ErpServiceContract/SalesInvoice",
            ReplyAction = AppStrings.Action,
            IsTerminating = false,
            IsInitiating = true,
            IsOneWay = false,
            AsyncPattern = false)]
        SalesInvoiceResponse SalesInvoice(SalesInvoiceRequest salesInvoiceRequest);

        [OperationContract(
            Action = "http://ErpSystemApi/ErpService/2020/ErpServiceContract/PurchaseOrder",
            ReplyAction = AppStrings.Action,
            IsTerminating = false,
            IsInitiating = true,
            IsOneWay = false,
            AsyncPattern = false)]
        PurchaseOrderResponse PurchaseOrder(PurchaseOrderRequest purchaseOrderRequest);

        [OperationContract(
            Action = "http://ErpSystemApi/ErpService/2020/ErpServiceContract/PurchaseInvoice",
            ReplyAction = AppStrings.Action,
            IsTerminating = false,
            IsInitiating = true,
            IsOneWay = false,
            AsyncPattern = false)]
        PurchaseInvoiceResponse PurchaseInvoice(PurchaseInvoiceRequest purchaseInvoiceRequest);
    }
}
