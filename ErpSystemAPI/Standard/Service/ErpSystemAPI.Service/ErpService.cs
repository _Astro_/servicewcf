﻿using ErpSystemAPI.AppLogger;
using ErpSystemAPI.MessageContracts.PurchaseInvoice;
using ErpSystemAPI.MessageContracts.PurchaseOrder;
using ErpSystemAPI.MessageContracts.SalesInvoice;
using ErpSystemAPI.MessageContracts.SalesOrder;
using ErpSystemAPI.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace ErpSystemAPI.Service
{
    /// <summary>
    /// Klasa główna serwisu z kontraktami
    /// </summary>
    public class ErpService : IErpService
    {
        private readonly ILogger _logger;

        /// <summary>
        /// Konstruktor serwisu
        /// </summary>
        /// <param name="logger">Klasa logowania informacji</param>
        public ErpService(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Kontrakt zamówienia sprzedaży
        /// </summary>
        /// <param name="salesOrderRequest">Klasa parametrów żądania</param>
        /// <returns></returns>
        public virtual SalesOrderResponse SalesOrder(SalesOrderRequest salesOrderRequest)
        {
            var response = new SalesOrderResponse();

            return response;
        }

        /// <summary>
        /// Kontrakt faktury sprzedaży
        /// </summary>
        /// <param name="salesInvoiceRequest">Klasa parametrów żądania</param>
        /// <returns></returns>
        public virtual SalesInvoiceResponse SalesInvoice(SalesInvoiceRequest salesInvoiceRequest)
        {
            var response = new SalesInvoiceResponse();

            return response;
        }

        /// <summary>
        /// Kontrakt zamówienia zakupu
        /// </summary>
        /// <param name="purchaseOrderRequest">Klasa parametrów żądania</param>
        /// <returns></returns>
        public virtual PurchaseOrderResponse PurchaseOrder(PurchaseOrderRequest purchaseOrderRequest)
        {
            var response = new PurchaseOrderResponse();

            return response;
        }

        /// <summary>
        /// Kontrakt faktury zakupu
        /// </summary>
        /// <param name="purchaseInvoiceRequest">Klasa parametrów żądania</param>
        /// <returns></returns>
        public virtual PurchaseInvoiceResponse PurchaseInvoice(PurchaseInvoiceRequest purchaseInvoiceRequest)
        {
            var response = new PurchaseInvoiceResponse();

            return response;
        }
    }
}
