﻿using ErpSystemAPI.Base.Resource;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace ErpSystemAPI.MessageContracts.PurchaseInvoice
{
    [MessageContract(WrapperName = AppStrings.PurchaseInvoiceRequestName, WrapperNamespace = AppStrings.ServiceBasicNamespace)]
    public class PurchaseInvoiceRequest
    {
    }
}
