﻿using ErpSystemAPI.Base.Abstract;
using ErpSystemAPI.Base.Resource;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace ErpSystemAPI.MessageContracts.PurchaseInvoice
{
    [MessageContract(WrapperName = AppStrings.PurchaseInvoiceResponseName, WrapperNamespace = AppStrings.ServiceBasicNamespace)]
    public class PurchaseInvoiceResponse : AResponseBase
    {
    }
}
