﻿using ErpSystemAPI.Base.Abstract;
using ErpSystemAPI.Base.Resource;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace ErpSystemAPI.MessageContracts.SalesOrder
{
    [MessageContract(WrapperName = AppStrings.SalesOrderResponseName, WrapperNamespace = AppStrings.ServiceBasicNamespace)]
    public class SalesOrderResponse : AResponseBase
    {
    }
}
