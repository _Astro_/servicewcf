﻿using ErpSystemAPI.Base.Resource;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace ErpSystemAPI.MessageContracts.SalesOrder
{
    [MessageContract(WrapperName = AppStrings.SalesOrderRequestName, WrapperNamespace = AppStrings.ServiceBasicNamespace)]
    public class SalesOrderRequest
    {
    }
}
