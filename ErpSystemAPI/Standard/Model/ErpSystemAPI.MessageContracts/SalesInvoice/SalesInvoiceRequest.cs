﻿using ErpSystemAPI.Base.Resource;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace ErpSystemAPI.MessageContracts.SalesInvoice
{
    [MessageContract(WrapperName = AppStrings.SalesInvoiceRequestName, WrapperNamespace = AppStrings.ServiceBasicNamespace)]
    public class SalesInvoiceRequest
    {
    }
}
