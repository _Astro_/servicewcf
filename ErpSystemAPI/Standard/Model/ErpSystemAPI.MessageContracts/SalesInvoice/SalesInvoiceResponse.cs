﻿using ErpSystemAPI.Base.Abstract;
using ErpSystemAPI.Base.Resource;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace ErpSystemAPI.MessageContracts.SalesInvoice
{
    [MessageContract(WrapperName = AppStrings.SalesInvoiceResponseName, WrapperNamespace = AppStrings.ServiceBasicNamespace)]
    public class SalesInvoiceResponse : AResponseBase
    {
    }
}
