﻿using ErpSystemAPI.Base.Abstract;
using ErpSystemAPI.Base.Resource;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace ErpSystemAPI.MessageContracts.PurchaseOrder
{
    [MessageContract(WrapperName = AppStrings.PurchaseOrderResponseName, WrapperNamespace = AppStrings.ServiceBasicNamespace)]
    public class PurchaseOrderResponse : AResponseBase
    {
    }
}
