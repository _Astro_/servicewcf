﻿using ErpSystemAPI.Base.Resource;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace ErpSystemAPI.MessageContracts.PurchaseOrder
{
    [MessageContract(WrapperName = AppStrings.PurchaseOrderRequestName, WrapperNamespace = AppStrings.ServiceBasicNamespace)]
    public class PurchaseOrderRequest
    {
    }
}
