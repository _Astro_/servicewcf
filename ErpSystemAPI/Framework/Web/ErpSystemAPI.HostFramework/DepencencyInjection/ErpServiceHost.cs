﻿using ErpSystemAPI.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace ErpSystemAPI.HostFramework.DepencencyInjection
{
    public class ErpServiceHost : ServiceHost
    {
        public ErpServiceHost(IErpService erpService, Type serviceType, params Uri[] baseAddresses)
        : base(serviceType, baseAddresses)
        {
            foreach (var cd in this.ImplementedContracts.Values)
            {
                cd.Behaviors.Add(new ErpServiceInstanceProvider(erpService));
            }
        }
    }
}