﻿using ErpSystemAPI.AppLogger;
using ErpSystemAPI.Service;
using ErpSystemAPI.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;

namespace ErpSystemAPI.HostFramework.DepencencyInjection
{
    /// <summary>
    /// Klasa fabryka dla hostowanego serwisu
    /// </summary>
    public class ErpServiceHostFactory : ServiceHostFactory
    {
        private readonly IErpService _erpService;

        /// <summary>
        /// Konstruktor domyślny z DI
        /// </summary>
        public ErpServiceHostFactory()
        {
            IoC.Register<ILogger, DefaultLogger>();
            IoC.Register<IErpService, ErpService>();
            _erpService = IoC.Resolve<IErpService>();
        }

        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            return new ErpServiceHost(_erpService, serviceType, baseAddresses);
        }
    }
}